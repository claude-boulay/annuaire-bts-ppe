-- phpMyAdmin SQL Dump
-- version 5.0.4deb2+deb11u1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mer. 21 fév. 2024 à 11:04
-- Version du serveur :  10.5.19-MariaDB-0+deb11u2
-- Version de PHP : 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `Annuaire`
--

-- --------------------------------------------------------

--
-- Structure de la table `AncienEtudiant`
--

CREATE TABLE `AncienEtudiant` (
  `etudiant_id` int(11) NOT NULL,
  `etudiant_nom` varchar(50) NOT NULL,
  `etudiant_prenom` varchar(50) NOT NULL,
  `etudiant_telephone` int(11) DEFAULT NULL,
  `etudiant_mail` varchar(50) NOT NULL,
  `etudiant_promo` varchar(50) DEFAULT NULL,
  `etudiant_travail` tinyint(4) DEFAULT NULL,
  `id_categorie` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `AncienEtudiant`
--

INSERT INTO `AncienEtudiant` (`etudiant_id`, `etudiant_nom`, `etudiant_prenom`, `etudiant_telephone`, `etudiant_mail`, `etudiant_promo`, `etudiant_travail`, `id_categorie`) VALUES
(1, 'stanieswi', 'justin', 513894777, 'justinstaniewski56@gmail.com', '2224', 1, 3),
(2, 'Collet', 'Theo', 637049159, 'theo.collet12@gmail.com', '2224', 1, 12),
(3, 'Raphael', 'Aladenise', 656892354, 'raphaelala2nise@gmail.com', '2224', 1, 12),
(4, 'expérience', 'test', 267548763, 'test@gmail.com', '2224', 0, 2),
(6, 'fgh', 'dsg', 44, 'fdh', '2325', 0, 5),
(7, 'claude', 'boulay', 2147483647, 'boulayclaude41@gmail.com', '2224', 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `Categorie_travail`
--

CREATE TABLE `Categorie_travail` (
  `id_categorie` int(11) NOT NULL,
  `Nom_categorie_emploi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `Categorie_travail`
--

INSERT INTO `Categorie_travail` (`id_categorie`, `Nom_categorie_emploi`) VALUES
(1, 'programmeur analyste'),
(2, 'développeur web'),
(3, 'programmeur d\'applications'),
(4, 'chargé d\'études informatiques'),
(5, 'développeur d\'application mobile'),
(6, 'technicien d\'études informatiques'),
(7, 'responsable de services applicatifs'),
(8, 'analyste d\'exploitation informatique'),
(9, 'gestionnaire de parc informatique'),
(10, 'administrateur systèmes et réseaux'),
(12, 'autres');

-- --------------------------------------------------------

--
-- Structure de la table `connexions`
--

CREATE TABLE `connexions` (
  `Id_user` int(50) NOT NULL,
  `MDP` varchar(1000) NOT NULL,
  `identifiant` varchar(50) NOT NULL,
  `nom_user` varchar(50) NOT NULL,
  `prenom_user` varchar(50) NOT NULL,
  `user_type` varchar(10) NOT NULL,
  `Suppress` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `connexions`
--

INSERT INTO `connexions` (`Id_user`, `MDP`, `identifiant`, `nom_user`, `prenom_user`, `user_type`, `Suppress`) VALUES
(1, '$2y$10$yP4DJz3XqIlkSFVBPjvLJuXi97Plek.7mGnR0le.5YFLYCsMbOAqq', 'cbmagala@gmail.com', 'boulay', 'claude', 'admin', 0),
(8, '', 'justinstaniewski56@gmail.com', 'Staniewski', 'Justin', 'etudiant', 0),
(11, '$2y$10$SgV5k8jJKd0OaR5mGW3XheKAOqNbkPQcTKn..3bq3QAP7LBP.kzMu', 'raphaelala2nise@gmail.com', 'ALADENISE', 'Raphael', 'etudiant', 1),
(13, '$2y$10$WwObU0XXrHGnBkf0qb4d7uo0ghtZ3rUjLBGD9Z34MAXn2123MLY/a', 'lisapeltier04@gmail.com', 'souris', 'verte', 'prof', 0),
(15, '$2y$10$y.Kwu6vX3I0oEJxY9UX.lOHP3/EOyefURU6xfaJUCUbtJLRDaVnfi', 'boulayclaude41@gmail.com', 'claude', 'boulay', 'etudiant', 0);

-- --------------------------------------------------------

--
-- Structure de la table `Organisation`
--

CREATE TABLE `Organisation` (
  `organisation_id` int(11) NOT NULL,
  `organisation_nom` varchar(50) NOT NULL,
  `organisation_adresse` varchar(50) DEFAULT NULL,
  `organisation_tel` int(11) DEFAULT NULL,
  `organisation_site` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `Organisation`
--

INSERT INTO `Organisation` (`organisation_id`, `organisation_nom`, `organisation_adresse`, `organisation_tel`, `organisation_site`) VALUES
(1, 'Cailleau', 'ZAC de la Grange', 549229685, ''),
(2, 'claude de france', 'ezfezfj', 4165117, ''),
(3, 'TubConcept', '7 place des kalamar', 216549528, 'https://google.com'),
(4, 'Leclerc', '7 rue d\'orléans', 956324285, 'http://leclerc.com'),
(5, 'lcdf', '9 rue de paris', 656462554, 'lcdf.com'),
(6, 'lcdf', '9 rue de paris', 656462554, 'lcdf.com'),
(7, 'lcdf', '9 rue de paris', 656462554, 'lcdf.com'),
(8, 'Leclerc', '2 rue d\'orléans', 64959659, 'http://test.com');

-- --------------------------------------------------------

--
-- Structure de la table `Professeur`
--

CREATE TABLE `Professeur` (
  `prof_id` int(11) NOT NULL,
  `prof_nom` varchar(50) NOT NULL,
  `prof_tel` int(11) NOT NULL,
  `prof_mail` varchar(50) NOT NULL,
  `prof_matiere` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Travailler`
--

CREATE TABLE `Travailler` (
  `travail_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  `etudiant_id` int(11) NOT NULL,
  `profession` varchar(50) NOT NULL,
  `annee_debut` date NOT NULL,
  `annee_fin` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `Travailler`
--

INSERT INTO `Travailler` (`travail_id`, `organisation_id`, `etudiant_id`, `profession`, `annee_debut`, `annee_fin`) VALUES
(1, 1, 1, 'Developpeur web', '2023-09-11', '2023-09-30'),
(2, 2, 2, 'test', '2023-10-11', '2023-10-31'),
(3, 2, 1, 'test', '2023-10-01', NULL),
(4, 3, 3, 'administrateur réseau', '2023-03-02', NULL),
(6, 7, 7, 'test', '2005-05-26', '2024-02-12'),
(7, 8, 7, 'technicien de surface', '2023-06-06', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `AncienEtudiant`
--
ALTER TABLE `AncienEtudiant`
  ADD PRIMARY KEY (`etudiant_id`),
  ADD KEY `id_categorie` (`id_categorie`);

--
-- Index pour la table `Categorie_travail`
--
ALTER TABLE `Categorie_travail`
  ADD PRIMARY KEY (`id_categorie`);

--
-- Index pour la table `connexions`
--
ALTER TABLE `connexions`
  ADD PRIMARY KEY (`Id_user`);

--
-- Index pour la table `Organisation`
--
ALTER TABLE `Organisation`
  ADD PRIMARY KEY (`organisation_id`);

--
-- Index pour la table `Professeur`
--
ALTER TABLE `Professeur`
  ADD PRIMARY KEY (`prof_id`);

--
-- Index pour la table `Travailler`
--
ALTER TABLE `Travailler`
  ADD PRIMARY KEY (`travail_id`),
  ADD KEY `Travailler_Organisation_FK` (`organisation_id`),
  ADD KEY `Travailler_AncienEtudiant0_FK` (`etudiant_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `AncienEtudiant`
--
ALTER TABLE `AncienEtudiant`
  MODIFY `etudiant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `Categorie_travail`
--
ALTER TABLE `Categorie_travail`
  MODIFY `id_categorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `connexions`
--
ALTER TABLE `connexions`
  MODIFY `Id_user` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `Organisation`
--
ALTER TABLE `Organisation`
  MODIFY `organisation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `Professeur`
--
ALTER TABLE `Professeur`
  MODIFY `prof_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Travailler`
--
ALTER TABLE `Travailler`
  MODIFY `travail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `AncienEtudiant`
--
ALTER TABLE `AncienEtudiant`
  ADD CONSTRAINT `fk_id_categorie` FOREIGN KEY (`id_categorie`) REFERENCES `Categorie_travail` (`id_categorie`);

--
-- Contraintes pour la table `Travailler`
--
ALTER TABLE `Travailler`
  ADD CONSTRAINT `Travailler_AncienEtudiant0_FK` FOREIGN KEY (`etudiant_id`) REFERENCES `AncienEtudiant` (`etudiant_id`),
  ADD CONSTRAINT `Travailler_Organisation_FK` FOREIGN KEY (`organisation_id`) REFERENCES `Organisation` (`organisation_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
