<?php

use App\Models\Organisation;

?>
<!doctype html>
<html lang="fr">

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="./img/favicon.png" type="image/png">
    <title>Annuaire des classe de BTS SIO</title>
    <!-- Bootstrap CSS -->
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/boostrap.css">
    <link rel="stylesheet" href="assets/css/boostrap-reboot.css">
    <link rel="stylesheet" href="assets/css/boostrap-grid.css">
    <link rel="stylesheet" href="assets/css/boostrap-utilities.css">
    <!-- main css -->
   
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
</head>

<body class="bg-body-secondary" >
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>

    <!--================Header Menu Area =================-->
    <header class="header_area bg-secondary">

        <div class="main_menu">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                        <ul class="nav navbar-nav menu_nav ml-auto">
                            <li class="nav-item  "><a class="nav-link text-white" href="gestionEleve">Accueil</a></li>
                        
                        <?php  if(isset($_SESSION["ID_USER"])){?>
                        <li class="nav-item submenu dropdown"> 
                            <a  class="nav-link dropdown-toggle text-white" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Menu</a>

                            </a>
                            <ul class="dropdown-menu">  
                                
                                <li class='nav-item'><a class='nav-link' href='viewAll'>Eleve</a></li>
                                
                                <li class='nav-item'><a class='nav-link' href='creaCompte'>Ajout de compte</a></li>
                                <li class='nav-item'><a class='nav-link' href="monCompte">Mon compte</a></li>
                                <?php if(isset($_SESSION["TYPE_USER"]) and $_SESSION["TYPE_USER"]=="admin"){
                                    echo "<li class='nav-item'><a class='nav-link' href='allUser'>Ensemble d'utilisateur</a></li>";
                                }?>
                                

                            </ul>
                                <?php }?>
                        
                        
                            
                        </li> 
                        </ul>   
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <div>
    
        <section>
            <div class=""><h1 class="text-center">Ancien Etudiant :</h1></div><br><br>
            <div class=" rounded text-center ">
                <br><p class="fs-3 text">Nom : <?=$eleve['etudiant_nom']?></p>
                <p class="text fs-3">Prenom : <?=$eleve["etudiant_prenom"]?></p>
                <p class="fs-3 text">Email: <?=$eleve["etudiant_mail"]?></p>
                <p class="fs-3 text">Telephone: <?=$eleve["etudiant_telephone"]?></p>
                <br>
                
            </div><br><br>
            <div class="bg-secondary container rounded border border-secondary border-4">
                <h1 class="text-center text-white">Historique des professions :</h1><br>
                <?php if(isset($eleve["etudiant_travail"])){
                    
                    foreach($travails as $travail){
                        $idOrg=$travail["organisation_id"];
                        
                        echo "<div class=' container-sm border border-body-secondary bg-white  rounded text-center text-black'><p> Profession : ".$travail['profession']."</p>";
                        if($travail["annee_fin"]==null){
                            $datedebut=new DateTime($travail["annee_debut"]);
                            echo "<p> Date d'embauche : ".$datedebut->format("d-m-Y")."</p>";

                        }else{
                            $datedebut=new DateTime($travail["annee_debut"]);
                            $datefin=new DateTime($travail["annee_fin"]);
                            $diff=date_diff($datedebut,$datefin);
                            echo "<p> Durée de l'emploi : ".$diff->days." Jours </p>";
                            echo "<p> Fin de l'emploi le ".$datefin->format('d-m-Y')."</p>";
                        }
                        $org=Organisation::find($idOrg);
                        echo "<p> Nom de l'organisation : ".$org["organisation_nom"]."</p>";
                        echo "<p> Adresse de l'organisation :".$org["organisation_adresse"]."</p>
                        <p> téléphone de l'organisation :".$org["organisation_tel"]."</p>";
                        if($org['organisation_site']!=null){
                            echo "<a href='".$org['organisation_site']."' target='_blank' class='text-white'>Site de l'organisation</a></div><br><br>";
                        }else{  
                            echo "</div><br><br>";
                        }
                    }
                    
                }?>
            </div>
        </section>
        <section>

        </section>
    </div>
