<center>
    <div>
        <?php csrf_field();?>
        <form action="creaCompte" method="post"><br><br><br>

        <label for="nom">Nom de l'utilisateur</label>
        <input type="text" pattern="^[a-zA-Z]{2,9}$" name="nom" id="nom" required><br><br>

        <label for="prenom">Prenom de l'utilisateur</label>
        <input type="text" name="prenom" pattern="^[a-zA-Z]{2,9}$" id="prenom" required><br><br>

        <label for="mail">Email</label>
        <input type="email"  pattern="^\w+(?:[-.+]\w+)*@\w+(?:[-.]\w+)*\.[a-z]{2,6}$" name="mail" id="mail" required><br><br>

        <h5>Type d'utilisateur : </h5>
        <label for="AE">Ancien Etudiant</label>
        <input type="radio" name=type id="AE" value="etudiant" required><br>
        <label for="prof">Professeur</label>
        <input type="radio" name="type" id="prof" value="prof" required><br>
        <label for="admin">Admin</label>
        <input type="radio" name="type" id="admin" value="admin" required><br><br>

        <input type="submit" value="Envoyer">
        </form>
    </div>
</center>