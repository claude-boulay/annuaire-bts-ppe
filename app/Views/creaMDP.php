<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="./img/favicon.png" type="image/png">
    <title>Annuaire des classe de BTS SIO</title>
    <!-- Bootstrap CSS -->
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/boostrap.css">
    <link rel="stylesheet" href="assets/css/boostrap-reboot.css">
    <link rel="stylesheet" href="assets/css/boostrap-grid.css">
    <link rel="stylesheet" href="assets/css/boostrap-utilities.css">
    <!-- main css -->
   
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
</head>
<center>
    <form method="post" action="creaMDP"><br><br>
        <h1>Entrez votre mot de passe pour valider
        la création de votre compte</h1>

        <p>merci de créer votre mot de passe avec au moins 8 caractères dont 1 majuscule, 1 minuscule , 1 chiffre et 1 caractère spéciaux</p>

        <label for="pass1">Entrez votre mot de passe</label>
        <input type="password" name="pass1" id="pass1" pattern="^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,}).{8,}$" required><br><br>
        <label for="pass2">Validez votre mot de passe</label>
        <input type="password" name="pass2" id="pass2" pattern="^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,}).{8,}$" required><br><br>

        <input type="hidden" name="identifiant" value=<?=$user[0]["identifiant"]?>>

        <input type="submit" id="valider" name="envoie" value="Valider">
    </form>

    <form method="post" action="creaMDP">
        <h1>Détruiser votre compte si vous ne désirez pas être enregistrées</h1>
        <label for="destruc">Destruction de votre compte</label><br><br>
        <input type="checkbox" id="destruc" name="destruc" required><br><br>
        <input type="hidden" name="identifiant" value=<?=$user[0]["identifiant"]?>>
        <input type="submit" id=détruire name="envoie" value="détruire">
    </form>
</center>

<script>
    const checkbox=document.getElementById("destruc");
    const pass1=document.getElementById("pass1");
    const pass2=document.getElementById("pass2");
    const détruire=document.getElementById("détruire");
    const valider=document.getElementById("valider");


    pass2.oninput = function () {
        if (pass2.value.length > 0) {
            password_verify_check = pass2.value == pass1.value;
            pass2.style = password_verify_check ? "border: solid 3px #00ff00;" : "border: solid 3px #ff0000;";
        } else {
            pass2.style = "border: solid 3px #d6d6d6;";
            password_verify_check = false;
        }
    }

        
        if(checkbox.checked===true){
            alider.setAttribute("disabled", "disabled");
            détruire.removeAttribute("disabled");
        }else{
            détruire.setAttribute("disabled", "disabled");
            valider.removeAttribute("disabled");
        }


    checkbox.onchange = function () {
        if(checkbox.checked===true){
            valider.setAttribute("disabled", "disabled");
            détruire.removeAttribute("disabled");
        }else{
            détruire.setAttribute("disabled", "disabled");
            valider.removeAttribute("disabled");
        }
    }
    


</script>