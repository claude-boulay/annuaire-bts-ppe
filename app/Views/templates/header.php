<?php
if(!isset($_SESSION)){
    //session_start();
}?>
<!doctype html>
<html lang="fr">

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="./img/favicon.png" type="image/png">
    <title>Annuaire des classe de BTS SIO</title>
    <!-- Bootstrap CSS -->
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/boostrap.css">
    <link rel="stylesheet" href="assets/css/boostrap-reboot.css">
    <link rel="stylesheet" href="assets/css/boostrap-grid.css">
    <link rel="stylesheet" href="assets/css/boostrap-utilities.css">
    <!-- main css -->
   
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <script   src  =  "  https://code.highcharts.com/highcharts.js  "  >  </script>
     <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
</head>

<body class="bg-body-secondary">
   

    <!--================Header Menu Area =================-->
    <header class="header_area bg-secondary">

        <div class="main_menu">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse offset text-white" id="navbarSupportedContent">
                        <ul class="nav navbar-nav menu_nav ml-auto">

                           
                        
                        <?php  if(isset($_SESSION["ID_USER"])){?>
                        <li class="nav-item"><a class="nav-link text-white" href="gestionEleve">Accueil</a></li>
                        <li class="nav-item submenu dropdown"> 
                            <a  class="nav-link dropdown-toggle text-white" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Menu</a>

                            </a>
                            <ul class="dropdown-menu bg-secondary text-white">  
                                
                                <li class='nav-item'><a class='nav-link text-white ' href='viewAll'>Eleve</a></li>
                                
                                
                                <li class='nav-item'><a class='nav-link text-white border-secondary' href="monCompte">Mon compte</a></li>
                                <?php if(isset($_SESSION["TYPE_USER"]) and $_SESSION["TYPE_USER"]=="admin"){
                                    ?><li class='nav-item'><a class='nav-link text-white' href='creaCompte'>Ajout de compte</a></li><?php
                                    echo "<li class='nav-item'><a class='nav-link text-white' href='allUser'>Ensemble d'utilisateur</a></li>";
                                }?>
                                <li class='nav-item'><a class='nav-link text-white' href='logout'>Déconnexion</a></li>

                                

                            </ul>
                                <?php }else{?>
                                    <li class="nav-item"><a class="nav-link" href="http://localhost/~claude.boulay/ppe/Annuaire_PPE/public/">Accueil</a></li>
                                    <?php }?>
                        </li> 
                        </ul>   
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <section class="banner_area bg-secondary">
        <div class="banner_inner d-flex align-items-center">
            <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
                <div class="container">
                    <div class="banner_content text-center">
                        <h2>Annuaire des classes de BTS SIO</h2>


                    </div>
                </div>
            </div>
        </div>
    </section>
