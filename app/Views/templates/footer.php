<br><br>
<footer class="container-fluid card-footer bg-secondary border-ligth fixed-bottom text-center">
    <div class="container-fluid text-white row text-center justify-content-md-center"> 
        <div class="col col-lg-2 container-fluid   start-0 ">
            
                <h5>Coordonnées</h5>
                <p>9 Av. de Paris, 41200 Romorantin-Lanthenay</p>
            
        </div>
        <div class="container-fluid text-white col col-md-auto position-relative    bottom-0 ">
            
                <h5>Contact</h5>
                <p>cbmagala@gmail.com</p>
            
        </div>
        <div class="container-fluid text-white col col-lg-2  position-relative  ">

            <h5>Représentant</h5>
            <p>1er année : M.Clavier <br>  2ème année : M.Poujol</p>
            
        </div>
        
    </div>
   
</footer> 