<br><br>


<center>
    <?php if(isset($suppression)){
        if($suppression==="suppression"){
            echo "<div class='alert alert-success' role='alert'> Le compte a bien été supprimé</div>";
        }
    }?>
    <input class="text-center" id="searchbar" onkeyup="search_user()" type="text" name="search" placeholder="cherche utilisateur..">

</center>
<?php foreach ($users as $user ) {
    
    $nom=$user["nom_user"];
    $prenom=$user["prenom_user"];
    $mail=$user["identifiant"];
    $type=$user["user_type"];
    $id=$user["Id_user"];
    $supress=$user["Suppress"];
    ?>
    <br><div class="user text-white text-center container bg-secondary border-2 border border-primary rounded-end rounded-start eleve <?php echo ($supress ? "bg-danger" : "") ?>" >
    <?=$nom?>     <?=$prenom?> <br> <?=$mail."<br>   type d'utilisateur = ".$type   ?> <br><button type="button" class="btn btn-primary"
     data-bs-toggle="modal" data-id=<?=$id?> data-bs-target="#exampleModal">Supprimer</button>      </div><br><br>
<?php }?>


<!-- modal pour la suppression-->
<div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Suppression d'un utilisateur</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>voulez vous réellement supprimez cet utilisateur ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" href="" id="validation" class="btn btn-primary">Supprimez</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
      </div>
    </div>
  </div>
</div>

<script>
    
    function search_user() {
    let input = document.getElementById('searchbar').value
    input=input.toLowerCase();
    let x = document.getElementsByClassName('user');
      
    for (i = 0; i < x.length; i++) { 
        if (!x[i].innerHTML.toLowerCase().includes(input)) {
            x[i].style.display="none";
        }
        else {
            x[i].style.display="list-item";                 
        }
    }
    }





modal=document.getElementById("exampleModal");
modal.addEventListener('shown.bs.modal',function(event){
    button=event.relatedTarget;
    let id=button.getAttribute("data-id");
    let suppression=document.getElementById("validation");
    console.log(id);
    suppression.setAttribute("href","deleteUser?id="+id);
})

button=document.getElementById("validation");
button.addEventListener('click',function(event){
    event.preventDefault();
    let id=event.target.getAttribute("href");
    window.location.href=id;
})


    
    
</script>