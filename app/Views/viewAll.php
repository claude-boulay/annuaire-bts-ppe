<?php use App\Models\AncienEtudiant;
use App\Models\Organisation;
use mitoteam\jpgraph\MtJpGraph;
include_once("../vendor/mitoteam/jpgraph/src/lib/jpgraph.php");
include_once("../vendor/mitoteam/jpgraph/src/lib/jpgraph_pie.php");

?>
<?php use App\Models\Travailler;?>


<center>
    <br>
    <h2> Dernier travail en date des anciens étudiants</h2>
    <br>
    <div class="table-responsive">
    <table class="table table-bordered table-responsive">
        <tr class="table-primary">
            <th>Nom de l'étudiant</th>
            <th>Prénom de l'étudiant</th>
            <th>Téléphone de l'étudiant</th>
            <th>E-mail de l'étudiant</th>
            <th>Promo de l'étudiant</th>
            <th>Profession</th>
            <th>Date début de la Profession</th>
            <th>Date Fin de profession</th>
            <th>Durée de la Profession (en jours)</th>
            <th>Nom de l'Organisation</th>
            <th>Adresse de l'Organisation</th>
            <th>Téléphone de l'Organisation</th>
            <th>Site de l'Organisation</th>
            
        </tr>
        <?php
        //création des données du tableau
      $Etudiants=AncienEtudiant::where("etudiant_travail",1)->get();
        foreach ($Etudiants as  $Etudiant) {
            //print_r($Etudiant);
            $travail=Travailler::where("etudiant_id",$Etudiant["etudiant_id"])->orderBy("annee_debut","desc")->first();
            $donnees=Organisation::where("organisation_id",$travail["organisation_id"])->get();
           
            //$travailID=$donnees[0]["pivot"]["travail_id"];
            
            //print_r($donnees[0]["pivot"]);
            echo"<br><br><tr class='table-success'><td>".$Etudiant["etudiant_nom"]."
            </td><td>".$Etudiant["etudiant_prenom"]."</td>
            <td>".$Etudiant["etudiant_telephone"]."</td>
            <td>".$Etudiant["etudiant_mail"]."</td>
            <td>".$Etudiant["etudiant_promo"]."</td>
            <td>".$travail["profession"]."</td>";

           
            $debut=new DateTime($travail["annee_debut"]);

            echo "<td>".$debut->format("d/m/Y")."</td>"
           ;
            if($travail["annee_fin"]!=null){
                $fin=new DateTime($travail["annee_fin"]);
                $diff=date_diff($fin,$debut);
                //var_dump($diff);
                echo " <td>".$fin->format("d/m/Y")."</td><td>".$diff->days." jours</td>";
            }else{
               
                $date=new DateTime();
                $diff=date_diff($date,$debut);
                //var_dump($diff);
                echo "<td>Toujours En cours</td><td>".$diff->days." jours</td>";
            }
            echo "<td>".$donnees[0]["organisation_nom"]."</td>
            <td>".$donnees[0]["organisation_adresse"]."</td>
            <td>".$donnees[0]["organisation_tel"]."</td>
            <td><a href='".$donnees[0]["organisation_site"]."'>".$donnees[0]["organisation_site"]."</a></td>
            
            

            </tr>";
        }
      //$travaileleve=$Etudiant[0]->Travailler()->get();
      
      //var_dump($donneEtudiant);
      //var_dump($travaileleve);
        ?>
    </table>
    </div>

<?php
$pa=AncienEtudiant::where("id_categorie",1)->get()->count("id_categorie");
$dv=AncienEtudiant::where("id_categorie",2)->get()->count("id_categorie");
$papp=AncienEtudiant::where("id_categorie",3)->get()->count("id_categorie");
$cei=AncienEtudiant::where("id_categorie",4)->get()->count("id_categorie");
$dam=AncienEtudiant::where("id_categorie",5)->get()->count("id_categorie");
$tei=AncienEtudiant::where("id_categorie",6)->get()->count("id_categorie");
$rsa=AncienEtudiant::where("id_categorie",7)->get()->count("id_categorie");
$aei=AncienEtudiant::where("id_categorie",8)->get()->count("id_categorie");
$gpi=AncienEtudiant::where("id_categorie",9)->get()->count("id_categorie");
$asr=AncienEtudiant::where("id_categorie",10)->get()->count("id_categorie");
$autres=AncienEtudiant::where("id_categorie",11)->get()->count("id_categorie");
$total=AncienEtudiant::all()->count("etudiant_id");
$categorieCount=[
    "programmeur analyste"=>$pa,
    "développeur web"=>$dv,
    "programmeur d'applications"=>$papp,
    "chargé d'études informatiques"=>$cei,
    "développeur d'applications"=>$dam,
    "technicien d'études informatiques"=>$tei,
    "responsable de services applicatifs"=>$rsa,
    "analyste d'exploitation informatique"=>$aei,
    "gestionnaire de parc informatique"=>$gpi,
    "administrateur sytèmes et réseaux"=>$asr,
    "autres"=>$autres
];
$categoriePercent=[];
foreach ($categorieCount as $key => $value) {
    $categoriePercent[]=$value/$total*100;
    $categrorieName[]=$key;
}







$largeur = 700;
$hauteur = 500;

// Initialisation du graphique
$graphe = new PieGraph($largeur, $hauteur);

// Creation du camembert
$camembert = new PiePlot($categoriePercent);
$camembert->SetLegends($categrorieName);
// Ajout du camembert au graphique
$graphe->add($camembert);
$graphe->legend->SetColumns(3);

// Ajout du titre du graphique
$graphe->title->Set("pourcentages des premiers emplois après le BTS");

// Affichage du graphique
$graphe->Stroke("img/firstEmploi.png");




?><br><br>
<img src="img/firstEmploi.png">


</div>
   
</center>



