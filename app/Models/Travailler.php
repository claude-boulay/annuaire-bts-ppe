<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Travailler extends Model{
    public $timestamps=false;
    
    protected $table="Travailler";

    protected $primaryKey="travail_id";
    
}