<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class AncienEtudiant extends Model{
    public $timestamps=false;

    protected $table="AncienEtudiant";

    protected $primaryKey="etudiant_id";

    public function Travailler(){
        return $this->belongsToMany(Organisation::class,Travailler::class,"organisation_id","travail_id","etudiant_id");
    }

    //retourne des organisations
       public function Organisation(){
        return $this->belongsToMany("App\Models\Organisation","Travailler","etudiant_id","organisation_id")->withPivot("profession","annee_debut","annee_fin");
    }

    public function addWork($idEtudiant,$idOrganisation,$profession,$annee_debut,$anne_fin){
        $travail=AncienEtudiant::find($idEtudiant);
        $travail->Organisation()->attach($idOrganisation,["profession"=>$profession,"annee_debut"=>$annee_debut,"annee_fin"=>$anne_fin]);
    }

    public function categoryWork(){
        return $this->belongsTo("App\Models\Category","id_categorie");
    }
}