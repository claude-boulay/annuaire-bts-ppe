<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Organisation extends Model{
    public $timestamps=false;
    
    protected $table="Organisation";

    protected $primaryKey="organisation_id";
}