<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Connexion extends Model{
    public $timestamps=false;

    protected $table="connexions";

    protected $primaryKey="Id_user";
}