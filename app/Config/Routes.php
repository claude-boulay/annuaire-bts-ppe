<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');
$routes->get("viewIdentification","Identification::identification");
$routes->get("viewAll","Home::Accueil",['filter'=>'Connecter']);
$routes->post("viewAll","Home::Connexion");
$routes->get('accueil',"Home::index");
$routes->get('creaCompte',"Identification::creaCompte",['filter'=>'Connecter']);
$routes->post("creaCompte","Compte::creation",['filter'=>['Connecter','Admin']]);
$routes->get("gestionEleve","Eleve::Affichage",['filter'=>'Connecter']);
$routes->get("eleve","Eleve::Information",['filter'=>'Connecter']);
$routes->get("creaMDP","Compte::creaMDP");
$routes->post("creaMDP","Compte::validMDP");
$routes->get("monCompte","Compte::monCompte",['filter'=>'Connecter']);
$routes->get('allUser',"Compte::allUser",['filter'=>['Connecter','Admin']]);
$routes->get('deleteUser',"Compte::deleteUser",['filter'=>['Connecter','Admin']]);
$routes->post("monCompte","Compte::monCompte",['filter'=>'Connecter']);
$routes->get("logout","Compte::logout",['filter'=>'Connecter']);


//routes Webservices
$routes->group("webservice",["filter"=>"JWT"],static function($routes){
$routes->get("getAncienEtudiants","WebService::getAncienEtudiants");
$routes->get("getAncienEtudiant/(:num)","WebService::getAncienEtudiant/$1");
$routes->get("getAncienEtudiant/(:any)","WebService::getAncienEtudiant2/$1");

$routes->get("getWork/(:num)","WebService::getWork/$1");
$routes->get("TrueUser/(:segment)/(:any)","WebService::TrueUser/$1/$2");
$routes->get("CategoryWork/(:num)","WebService::CategoryWork/$1");
$routes->get("getProfil","WebService::getProfil");
$routes->post("addInformation","WebService::addInformation");
$routes->post("createJob","WebService::addJob");
$routes->get("showYoursJobs","WebService::getYoursJobs");
$routes->post("getAJob","WebService::getAJob");
$routes->post("UpdateJob","WebService::updateJob");
$routes->post("SuppressProfil","WebService::ASuprimmer");
$routes->post("allEtudiantJobs","WebService::allEtudiantJobs");
$routes->post("percentWorkGraph","WebService::percentWorkGraph");
});

$routes->post("webservice/connexion","WebService::connexion");