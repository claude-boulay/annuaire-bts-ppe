<?php
namespace App\Controllers;

use App\Models\AncienEtudiant;
use App\Models\Category;
use App\Models\Connexion;
use App\Models\Organisation;
use DateTime;
use Exception;
use Firebase\JWT\JWT;
use JpGraphError;
use mitoteam\jpgraph\MtJpGraph;
use PieGraph;
use PiePlot;

include_once("../vendor/mitoteam/jpgraph/src/lib/jpgraph.php");
include_once("../vendor/mitoteam/jpgraph/src/lib/jpgraph_pie.php");



class WebService extends BaseController{

    public function connexion(){
        $identifiant=$this->request->getVar('Identifiant');
        $mdp=$this->request->getVar('mdp');
        $iat=time();
        $nbf=$iat-1;
        $exp=$iat+3600*24;
        $payload=array(
            "iss"=>"SioConnect.com",
            "aub"=>"mobile user",
            "iat"=>$iat,
            "nbf"=>$nbf,
            "exp"=>$exp,
            "data"=>$identifiant
        );

        try{
            $user=Connexion::where('identifiant',$identifiant)->first();
            $pass=$user["MDP"];
            $authentification=password_verify($mdp,$pass);
            if($authentification){
                $token=JWT::encode($payload,getenv("JWT_SECRET"),"HS256");
                $response=['message'=>"Login Successful",
                'token'=>$token];
                return $this->response->setJSON($response);
            }else{
                $token=['message'=>"Login failed", "error"=>$authentification];
                return json_encode($token);
            }

        }catch(Exception $e){
            $token=['message'=>"Login failed","error"=>$e,$identifiant=>$mdp,"password"=>$authentification];
            return json_encode($token);
        };
    }

    public function getAncienEtudiants() {
        $etudiants=AncienEtudiant::all();
        return $this->response->setJSON(json_encode($etudiants,JSON_UNESCAPED_UNICODE));
    }

    public function getAncienEtudiant(int $id=null){
        if($id!==null){
            $etudiant=AncienEtudiant::find($id);
            return json_encode($etudiant,JSON_UNESCAPED_UNICODE);
        }else{
            return null;
        }
    }

    public function getAncienEtudiant2(string $mail=null){
       if ($mail!==null){ 
            $etudiant=AncienEtudiant::where("etudiant_mail",$mail)->get();
            return json_encode($etudiant,JSON_UNESCAPED_UNICODE);
        }else{
            return null;
        }
    }

    public function getWork($idetudiant){
        $etudiant=AncienEtudiant::find($idetudiant);
        $work=$etudiant->Organisation()->get();
        return json_encode($work,JSON_UNESCAPED_UNICODE);
    }

    public function TrueUser($identifiant,$mdp){
        $user=Connexion::where("identifiant",$identifiant)->first();
        if($user!==null){
            
            $mdpcryptée=$user->MDP;
            if(password_verify($mdp,$mdpcryptée))
            {
                $user->MDP=null;
                $send=["success"=>true,"user"=>$user];
                return $this->response->setJSON(json_encode($send,JSON_UNESCAPED_UNICODE));
            }else{
                $send=["success"=>false,"user"=>null];
                return $this->response->setJSON(json_encode($send,JSON_UNESCAPED_UNICODE));
            }
        }else{
            $send=["success"=>false,"user"=>null];
            return $this->response->setJSON(json_encode($send,JSON_UNESCAPED_UNICODE));
        }

    }

    public function CategoryWork($id){
        $etudiant=AncienEtudiant::find($id);
        $category=$etudiant->CategoryWork()->find($etudiant->id_categorie);
        return json_encode($category,JSON_UNESCAPED_UNICODE);
    }

    public function getProfil(){
        helper("jwt");
        $user=getConnectedUser();
        $user->MDP=null;
        $etu=getConnectedEtu();
        
        return $this->response->setJSON(json_encode([$user,$etu],JSON_UNESCAPED_UNICODE));
    }

    public function addInformation(){
        if($this->validate([
            'nom' => 'required|min_length[2]|max_length[50]','prenom' => 'required|min_length[2]|max_length[50]',
                'email' => 'required|min_length[4]|max_length[50]|valid_email','promo'=> 'required|numeric',"tel"=>'numeric'
        ])){
            if($this->validate(["id_cat"=>"required|numeric"])){
                $id_cat=$this->request->getVar("id_cat")+1;
                if($id_cat==10){
                    $id_cat=12;
                }
            }else{
                $id_cat=12;
            }

            $email=$this->request->getVar("email");
            $tel=$this->request->getVar("tel");
            $nom=$this->request->getVar("nom");
            $prenom=$this->request->getVar("prenom");
            $promo=$this->request->getVar("promo");
       
            helper("jwt");
            $user=getConnectedUser();
            if($user->identifiant==$email){
                $chgmt="mineur";
            }else{
                $chgmt="important";
            }
            $user->identifiant=$email;
            $user->nom_user=$nom;
            $user->prenom_user=$prenom;
            $etu=getConnectedEtu();
            $etu->etudiant_nom=$nom;
            $etu->etudiant_prenom=$prenom;
            $etu->etudiant_telephone=$tel;
            $etu->etudiant_promo=$promo;
            $etu->etudiant_mail=$email;
            $etu->id_categorie=$id_cat;

            $user->save();
            $etu->save();

        return $this->response->setJSON(json_encode(["retour"=>$chgmt]));
        }else{
            return $this->response->setJSON(json_encode(["retour"=>"incorrecte"]));
        }

    }

    public function addJob(){
        $etu=getConnectedEtu();
       
        if($this->validate(["emploi"=>"required",
        "entreprise"=>"required",
        "adresse"=>"required",
        "startDate"=>"required",
        "tel"=>"required|numeric",

        ])){

            $emploi=$this->request->getVar("emploi");
            $entreprise=$this->request->getVar("entreprise");
            $adresse=$this->request->getVar("adresse");
            $startDate=$this->request->getVar("startDate");
            $trsfDate=new DateTime($startDate);
            $startDate=$trsfDate->format("Y-m-d");
            $tel=$this->request->getVar("tel");
           

            if($this->validate([
                "site"=>"required"
            ])){
                $site=$this->request->getVar("site");
                if($this->request->getVar("endDate")){
                    $endate=$this->request->getVar("endDate");
                    $trsfDate=new DateTime($endate);
                    $endate=$trsfDate->format("Y-m-d");
                    $org=new Organisation();
                    $org->organisation_nom=$entreprise;
                    $org->organisation_adresse=$adresse;
                    $org->organisation_tel=$tel;
                    $org->organisation_site=$site;
                    $org->save();
                    $etu->addWork($etu->etudiant_id,$org->organisation_id,$emploi,$startDate,$endate);
                }else{
                    $endate=null;
                    $org=new Organisation();
                    $org->organisation_nom=$entreprise;
                    $org->organisation_adresse=$adresse;
                    $org->organisation_tel=$tel;
                    $org->organisation_site=$site;
                    $org->save();
                    $etu->addWork($etu->etudiant_id,$org->organisation_id,$emploi,$startDate,$endate);

                }


            }else if($this->validate([
                "endDate"=>"required"
            ])){
                $site=null;
                $endate=$this->request->getVar("endDate");
                $trsfDate=new DateTime($endate);
                $endate=$trsfDate->format("Y-m-d");
                $org=new Organisation();
                $org->organisation_nom=$entreprise;
                $org->organisation_adresse=$adresse;
                $org->organisation_tel=$tel;
                $org->organisation_site=$site;
                $org->save();
                $etu->addWork($etu->etudiant_id,$org->organisation_id,$emploi,$startDate,$endate);

            }else{
                $site=null;
                $endate=null;
                $org=new Organisation();
                $org->organisation_nom=$entreprise;
                $org->organisation_adresse=$adresse;
                $org->organisation_tel=$tel;
                $org->organisation_site=$site;
                $org->save();
                $etu->addWork($etu->etudiant_id,$org->organisation_id,$emploi,$startDate,$endate);

            }
            $etu->etudiant_travail=1;
            $etu->save();
            return $this->response->setJSON(json_encode(["retour"=>"success"]));
        }else{
            return $this->response->setJSON(json_encode(["retour"=>"failure"]));
        }
    }

    public function getYoursJobs(){
        $etu=getConnectedEtu();
        if($etu->etudiant_travail!=0){
            $travails=$etu->Organisation()->get();
           return $this->response->setJSON(json_encode(["travails"=>$travails,"retour"=>true]));

        }else{
            return $this->response->setJSON((Json_encode(["retour"=>"Null"])));
        }
    }

    public function getaJob(){
        $etu=getConnectedEtu();
        $idorg=$this->request->getVar("idOrg");
        $travail=$etu->Organisation()->where("Organisation.organisation_id",$idorg)->get();
        return $this->response->setJSON(json_encode($travail));
    }

    public function UpdateJob(){
        $etu=getConnectedEtu();
        if($this->validate(["telorg"=>"required|numeric","idorg"=>"required|numeric"])){
            $telorg=$this->request->getVar("telorg");
            $idorg=$this->request->getVar("idorg");
            $siteorg=null;
            $finTravail=null;
            if($this->validate(["siteorg"=>"required"])){
                $siteorg=$this->request->getVar("siteorg");
            }
            if($this->validate(["finTravail"=>"required"])){
                $fin=new DateTime($this->request->getVar(("finTravail")));
                $finTravail=$fin->format('Y-m-d');
            }
            $emploi=$etu->Organisation()->updateExistingPivot($idorg,["annee_fin"=>$finTravail
            ]);
            $org=Organisation::where("organisation_id",$idorg)->first();
            $org->organisation_tel=$telorg;
            $org->organisation_site=$siteorg;
            $org->save();
            return $this->response->setJSON(json_encode(["retour"=>true]));


        }else{
            return $this->response->setJSON((Json_encode(["retour"=>"Failed"])));
        }

    }

    public function ASuprimmer(){
        $user=getConnectedUser();
        if($user->Suppress==0){
            $user->Suppress=1;
            $user->save();
            return $this->response->setJSON(json_encode(["retour"=>true]));
        }else{
            $user->Suppress=0;
            $user->save();
            return $this->response->setJSON(json_encode(["retour"=>false]));
        }
        
    }

    public function allEtudiantJobs(){
        if($this->validate(["id"=>"required|numeric"])){
            $id=$this->request->getVar("id");
            $etu=AncienEtudiant::find($id);
            $travail=$etu->Organisation()->get();
            return $this->response->setJSON(json_encode(["retour"=>true,"etudiant"=>$etu,"emplois"=>$travail]));
        }else{
            return $this->response->setJSON(json_encode(["retour"=>false]));
        }
    }
    //envoie de l'image de pourcentage des premiers emploi à l'appli mobile.
    public function percentWorkGraph(){
        try{
        $pa=AncienEtudiant::where("id_categorie",1)->get()->count("id_categorie");
        $dv=AncienEtudiant::where("id_categorie",2)->get()->count("id_categorie");
        $papp=AncienEtudiant::where("id_categorie",3)->get()->count("id_categorie");
        $cei=AncienEtudiant::where("id_categorie",4)->get()->count("id_categorie");
        $dam=AncienEtudiant::where("id_categorie",5)->get()->count("id_categorie");
        $tei=AncienEtudiant::where("id_categorie",6)->get()->count("id_categorie");
        $rsa=AncienEtudiant::where("id_categorie",7)->get()->count("id_categorie");
        $aei=AncienEtudiant::where("id_categorie",8)->get()->count("id_categorie");
        $gpi=AncienEtudiant::where("id_categorie",9)->get()->count("id_categorie");
        $asr=AncienEtudiant::where("id_categorie",10)->get()->count("id_categorie");
        $autres=AncienEtudiant::where("id_categorie",11)->get()->count("id_categorie");
        $total=AncienEtudiant::all()->count("etudiant_id");
        $categorieCount=[
            "programmeur analyste"=>$pa,
            "développeur web"=>$dv,
            "programmeur d'applications"=>$papp,
            "chargé d'études informatiques"=>$cei,
            "développeur d'applications"=>$dam,
            "technicien d'études informatiques"=>$tei,
            "responsable de services applicatifs"=>$rsa,
            "analyste d'exploitation informatique"=>$aei,
            "gestionnaire de parc informatique"=>$gpi,
            "administrateur sytèmes et réseaux"=>$asr,
            "autres"=>$autres
        ];
        $categoriePercent=[];
        foreach ($categorieCount as $key => $value) {
            $categoriePercent[]=$value/$total*100;
            $categrorieName[]=$key;
        }
        $largeur = 700;
        $hauteur = 500;
        
        // Initialisation du graphique
        $graphe = new PieGraph($largeur, $hauteur);
        
        // Creation du camembert
        $camembert = new PiePlot($categoriePercent);
        $camembert->SetLegends($categrorieName);
        // Ajout du camembert au graphique
        $graphe->add($camembert);
        $graphe->legend->SetColumns(3);
        
        // Ajout du titre du graphique
        $graphe->title->Set("pourcentages des premiers emplois après le BTS");
        // Affichage du graphique
        //$graphe->Stroke("img/firstEmploi.png");
        
        return $this->response->setJSON(json_encode(["url"=>base_url()."img/firstEmploi"]));
    }catch(Exception $e){
        var_dump($e);
    }

    }
}   