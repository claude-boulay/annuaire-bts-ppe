<?php

namespace App\Controllers;

use App\Models\AncienEtudiant;
use App\Models\Connexion;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;


class Compte extends BaseController
{
    public function creation(){
        if($this->request->getMethod()=== "post"){
            if($this->validate([
                'nom' => 'required|min_length[2]|max_length[50]','prenom' => 'required|min_length[2]|max_length[50]',
                'mail' => 'required|min_length[4]|max_length[50]|valid_email|is_unique[connexions.identifiant]','type'=> 'required']))
            {
                $typeUser=$this->request->getPost("type");
                $nom=$this->request->getPost("nom");
                $prenom=$this->request->getPost("prenom");
                $mail=(string)$this->request->getPost("mail");

                $user=new Connexion();
                
                $user->identifiant=$mail;
                $user->nom_user=$nom;
                $user->prenom_user=$prenom;
                $user->user_type=$typeUser;

                $subject="Création d'un compte pour l'Annuaire des BTS SIO";
                //message a modifier une fois la page de connexion réalisée sur l'apli android
                $message="Bonjour M/Mme $nom $prenom vous trouvez dans le lien ci-dessus la page de connexion afin de créer votre passe.\n Afin de pouvoir accéder à l'Annuaire des BTS SIO du Lycée Claude de France :\n
                 ";
              
                $Envoiemail=new PHPMailer;
                        $Envoiemail->IsSMTP();
                        $Envoiemail->Host='in-v3.mailjet.com';
                        $Envoiemail->SMTPAuth=true;
                        $Envoiemail->SMTPSecure = 'tls';
                        $Envoiemail->Port = 587;
                        $Envoiemail->Username='93cfd70b756e3c3f93966e3b67faad6f' ;
                        $Envoiemail->Password='a87b731488ff31a210a38f0115a36e4e' ;
                        $Envoiemail->FromName ="Boulay Claude";
                        $Envoiemail->From='varanraptor41@gmail.com';
                        $Envoiemail->AddAddress($mail);
                        $Envoiemail->Subject=$subject;

                switch ($typeUser) {
                    case 'admin':
                        $Envoiemail->Body=$message."http://192.168.0.106/~claude.boulay/ppe/Annuaire_PPE/public/creaMDP?mail=$mail";
                        $result=true;
                       
                        break;

                    case "prof":  
                        
                        $Envoiemail->Body=$message."http://192.168.0.106/~claude.boulay/ppe/Annuaire_PPE/public/creaMDP?mail=$mail";
                        $result=true;
                        break;

                    case 'etudiant':
                        $Envoiemail->Body=$message."http://192.168.0.106/~claude.boulay/ppe/Annuaire_PPE/public/creaMDP?mail=$mail";
                        $etu=new AncienEtudiant();
                        $etu->etudiant_mail=$mail;
                        $etu->etudiant_nom=$nom;
                        $etu->etudiant_prenom=$prenom;
                        $etu->etudiant_travail=0;
                        $etu->id_categorie=12;
                        $etu->save();
                        $result=true;
                        break;
                    
                    default:
                        $result=False;
                        break;
                }
                
                $Envoiemail->IsHTML(true);
                if(!$Envoiemail->Send()){echo $Envoiemail->ErrorInfo;}else{}
                        
               
                if($result==true){
                    $user->save();
                    echo view("templates/header");
                    echo view("resultat/Validation");
                    return view("templates/footer");
                }else{
                    echo view("templates/header");
                    echo view("resultat/Echec");
                    return view("templates/footer");
                }

            }else{
                $data["validation"]=$this->validator;
                    echo view("templates/header");
                    echo view("resultat/Echec");
                    return view("templates/footer");
            }

        }
    }

    public function creaMDP(){
        $mail=$_GET["mail"];
        if($user=Connexion::where("identifiant",$mail)->get()){
            if($user[0]["MDP"]!=null){
                if($user[0]["user_type"]=="etudiant"){
                    echo "Merci pour votre création de compte veuillez utiliser l'application SIOConnect pour finaliser vos information et consultez les débouchées ";
                }else{
                    return view("index");
                }
            
            }
            else{
            $data["user"]=$user;
            return(view("creaMDP",$data));
            }
        }else {
            header("Location: https://www.google.com/");
        }
                
    }

    public function validMDP(){
        if($this->request->getMethod()=== "post"){
            if($this->request->getPost("envoie")==="Valider"){
                if($this->validate([
                    'pass1' => 'required|min_length[4]|max_length[50]','pass2' => 'required|min_length[4]|max_length[50]|matches[pass1]',
                    ]))
                {
                    $user=Connexion::where("identifiant",$this->request->getPost("identifiant"))->first();
                    $MDP=(string)$this->request->getPost("pass1");
                    $hash=password_hash($MDP,PASSWORD_BCRYPT);
                    $user->MDP=$hash;
                    
                    $user->save();
                    //session()
                    if($user["user_type"]=="etudiant"){
                        echo "Merci pour votre création de compte veuillez utiliser l'application SIOConnect pour finaliser vos information et consultez les débouchées ";
                    }else{
                        $_SESSION["ID_USER"]=$user["Id_user"];
                        $_SESSION["NOM_USER"]=$user["nom_user"];
                        $_SESSION["PRENOM_USER"]=$user["prenom_user"];
                        $_SESSION["EMAIL_USER"]=$user["identifiant"];
                        $_SESSION["TYPE_USER"]=$user["user_type"];

                        echo view("templates/header");
                   
                        echo view("viewAll",$_SESSION);
                        return view("templates/footer");
                    }
                    

                }else {
                    echo view("creaMDP?mail=".$this->request->getPost("identifiant"));
                }
            }elseif ($this->request->getPost("envoie")==="détruire") {
                $user=Connexion::where("identifiant",$this->request->getPost("identifiant"))->first();
                $user->delete();
                var_dump($user);
                session_destroy();
                header("Location: https://www.google.com");
            } 
       
        }

    }
    public function monCompte(){
        if($this->request->getMethod()=== "post"){
            if($this->validate(['identifiant' =>'required|min_length[4]|max_length[50]|valid_email|is_unique[connexions.identifiant]'])){
                $user=Connexion::where("identifiant",$_SESSION["EMAIL_USER"])->first();
                $user->identifiant=$this->request->getPost("identifiant");
                $user->save();
                echo view("templates/header");
                return view("monCompte");
                //rajout d'un envoie de notif au admin
                
            }elseif ($this->request->getPost("suppress")==="yes") {
                $user=Connexion::where("identifiant",$this->request->getPost("identifiant"))->first();
                $user->delete();
                
                session_destroy();
                return redirect()->to("https://www.google.com");
                //rajout d'un envoie de notif au admin
            }

        }else {
            echo view("templates/header");
            echo view("monCompte");  
            return view("templates/footer");
        }
    }

    public function allUser(){
        session();
        echo view("templates/header");
        $users=Connexion::all();
        $data["users"]=$users;
        echo view("allUser",$data);
        return view("templates/footer");
    }

    public function deleteUser(){
        $id=$_GET["id"];
        $user=Connexion::find($id);
        if($user->type_user=="etudi" or $user->type_user=="etudiant"){
            $etudiant=AncienEtudiant::where("etudiant_mail",$user->identifiant)->first();
            $etudiant->delete();
            $user->delete();
            echo view("templates/header");
       
        }else{
            $user->delete();
            echo view("templates/header");
        
        }
        $users=Connexion::all();
        $data["users"]=$users;
        $data["suppression"]="suppression";
        echo view("allUser",$data);
        return view("templates/footer");
    }

    public function logout(){
        session_destroy();
        return redirect()->to("/");
    }

}