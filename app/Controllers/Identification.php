<?php

namespace App\Controllers;

class Identification extends BaseController
{

    public function identification(){
        echo view("templates/header");
        echo view("viewIdentification");
        return view("templates/footer");
    }

    public function creaCompte(){
        //session_start();
        echo view("templates/header");
        echo view("creaCompte");
        return view("templates/footer");
    }
}