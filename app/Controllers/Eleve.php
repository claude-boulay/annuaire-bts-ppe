<?php

namespace App\Controllers;

use App\Models\AncienEtudiant;
use App\Models\Travailler;

class Eleve extends BaseController
{
    public function Affichage(){
        //session_start();
        $eleve=AncienEtudiant::all();

        $data["eleve"]=$eleve;

        echo view("templates/header");
        echo view("Eleves",$data);
        return view("templates/footer");
    }

    public function Information(){
        
        $id=$_GET["id"];
        $eleve=AncienEtudiant::find($id);
        $data["eleve"]=$eleve;
        if($eleve["etudiant_travail"]){
            $travail=Travailler::where("etudiant_id",$id)->get();
            $data["travails"]=$travail;
            
        }
        return view("Eleve",$data);
    }
}