<?php

namespace App\Controllers;

use App\Models\Connexion;
use App\Models\AncienEtudiant;
use Exception;

class Home extends BaseController
{
    public function index(): string
    {
        /*if($_SESSION["ID_USER"]!=null){
            echo view("templates/header");
            echo view("viewAll.php");
        }*/
        return view('index');
    }

    function afficherAll()
{
    return view("viewAll");
}

    function Connexion(){
        if($this->request->getMethod()=== "post"){
            
            $identifiant=$this->request->getPost("identifiant");
            $mdp_entrer=$this->request->getPost("mdp");
            $mdp_entrer=(string)$mdp_entrer;
            $verif=false;
            try{
                 $user=Connexion::where("identifiant",$identifiant)->get();
                 $verif=password_verify($mdp_entrer,$user[0]["MDP"]);
                
            }catch(Exception $e){
                $data["echec"]="Echec de l'authentification";
                echo view("templates/header");
                echo $e;
                return view("viewIdentification",$data);
            }
            if($verif==true){
                    //session()
                    $_SESSION["ID_USER"]=$user[0]["Id_user"];
                    $_SESSION["NOM_USER"]=$user[0]["nom_user"];
                    $_SESSION["PRENOM_USER"]=$user[0]["prenom_user"];
                    $_SESSION["EMAIL_USER"]=$user[0]["identifiant"];
                    $_SESSION["TYPE_USER"]=$user[0]["user_type"];

                    echo view("templates/header");
                    $eleve=AncienEtudiant::all();

                    $data["eleve"]=$eleve;
                   
                    echo view("Eleves",$data);
                    return view("templates/footer");
                }else{
                    $data["echec"]="Echec de l'authentification";
                    echo view("templates/header");
                    echo view("viewIdentification",$data);
                    return view("templates/footer");
                }
           
            //var_dump($mdp_base[0]["MDP"]) ;
            
        }
    }

    function Accueil(){
        echo view("templates/header");
        echo view("viewAll");
        return view("templates/footer");
    }

}
