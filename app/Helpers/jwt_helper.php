<?php

use App\Models\AncienEtudiant;
use App\Models\Connexion;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

/*function getJWTFromRequest($authHeader){
    if(is_null($authHeader)){
        throw new Exception('missing or invalid JWT in request');
    }
    return explode(' ', $authHeader)[1];
}*/
 service("eloquent");
function validateJWTFromRequest(string $encodedToken){
    $key=new Key(getenv("JWT_SECRET"),'HS256');
    $decodedToken=JWT::decode($encodedToken,$key);
    
    $user=Connexion::where("identifiant",$decodedToken->data)->first();
  
  
   //$user=Utilisateur::where("mail_user","=","varanraptor41@gmail.com")->first();
   
   
   if($user!=null){
        return true;
    }else{
        return json_encode(["retour"=>"Invalid JWT Token"]);
    }
}

function getConnectedUser(){
    $header= getallheaders()["Authorization"];
    $decode=JWT::decode($header,new Key(getenv("JWT_SECRET"),'HS256'));
    $mail=$decode->data;
    $user=Connexion::where("identifiant",$mail)->first();
    return $user;
}

function getConnectedEtu(){
    $header=getallheaders()["Authorization"];
    $decode=JWT::decode($header,new Key(getenv("JWT_SECRET"),'HS256'));
    $mail=$decode->data;
    $etu=AncienEtudiant::where("etudiant_mail",$mail)->first();
    return $etu;
}